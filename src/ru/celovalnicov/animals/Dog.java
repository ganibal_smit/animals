package ru.celovalnicov.animals;

/**
 * Created by ог on 16.10.2014.
 */
public class Dog implements Animals {
    @Override
    public String getName() {
        return "Собака";
    }

    @Override
    public String getVoice() {
        return "Гав-гав";
    }
}
