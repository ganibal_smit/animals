package ru.celovalnicov.animals;

/**
 * Created by ог on 16.10.2014.
 */
public class Cat implements Animals {
    @Override
    public String getName() {
        return "Кошка";
    }

    @Override
    public String getVoice() {
        return "Мяу-мяу";
    }
}
