package ru.celovalnicov.animals;

/**
 * Created by ог on 16.10.2014.
 */
public class main {
    public static void main(String...args)
    {
        Dog dog=new Dog();
        print(dog);
        Cat cat=new Cat();
        print(cat);
        Pig pig=new Pig();
        print(pig);
        Chicken  chicken=new  Chicken();
        print(chicken);
    }
    private  static void print(Animals animals){
        String str =
                animals.getName() +
                        " Говорит " +
                        animals.getVoice();
        System.out.println(str);
    }
}
